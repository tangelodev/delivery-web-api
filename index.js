'use strict';

var restify = require('restify');
var bunyan = require('bunyan');
var restifyValidator = require('restify-validator');
var routes = require('./routes');
var errorHandler = require('./modules/error-handler');

var log = bunyan.createLogger({
    name: 'delivery-web-api',
    level: 'info',
    stream: process.stdout,
    serializers: bunyan.stdSerializers
});

var server = restify.createServer({name: 'delivery-web-api', log: log});

server.use(restify.bodyParser());
server.use(restify.queryParser());
server.use(restify.gzipResponse());
server.pre(restify.CORS());
server.pre(restify.pre.sanitizePath());
server.use(restifyValidator);
server.on('after', restify.auditLogger({log: log}));

errorHandler(server);
routes(server);

server.listen(8080, function () {
    log.info('%s listening at %s', server.name, server.url);
});

module.exports = server;