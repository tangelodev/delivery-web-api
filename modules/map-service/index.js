'use strict';

var mongojs = require('mongojs');
var db = mongojs('delivery-db');
var Q = require('q');
var _ = require('lodash');

module.exports = function () {
    return {
        saveOrUpdate: saveOrUpdate,
        getLocations: getLocations,
        findWays: findWays
    };

    function saveOrUpdate(map) {
        db.collection('maps').update(
            {origin: map.origin, destination: map.destination},
            {$set: {distance: map.distance}, $setOnInsert: {origin: map.origin, destination: map.destination}},
            {upsert: true});
    }

    function getLocations() {
        var deferred = Q.defer();
        db.collection('maps').find({}, {origin: 1, destination: 1},
            function (err, maps) {
                if (err) {
                    deferred.reject(err);
                } else {
                    var locations = [];
                    maps.forEach(function (map) {
                        locations = locations.concat([map.origin, map.destination]);
                    });
                    locations = _.uniq(locations);
                    deferred.resolve(locations);
                }
            });
        return deferred.promise;
    }

    function findWays(location) {
        var deferred = Q.defer();
        db.collection('maps').find({$or: [{origin: location}, {destination: location}]},
            function (err, maps) {
                if (err) {
                    deferred.reject(err);
                } else {
                    var ways = {};
                    var origin = {};
                    maps.forEach(function (map) {
                        if (map.origin === location) {
                            ways[map.destination] = map.distance;
                        } else {
                            ways[map.origin] = map.distance;
                        }
                    });
                    origin[location] = ways;
                    deferred.resolve(origin);
                }
            });
        return deferred.promise;
    }
};