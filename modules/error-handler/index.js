'use strict';

module.exports = function (server) {
    server.on('uncaughtException', function (req, res, err) {
        console.log('Error!');
        console.log(err);
        res.send(500, {success: false});
    });
};