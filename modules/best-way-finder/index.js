'use strict';

var mapService = require('./../map-service')();
var _ = require('lodash');
var Q = require('q');

module.exports = function () {
    return {
        findWay: findWay
    };

    function findWay(origin, destination) {
        var deferred = Q.defer();
        var waysToVerify = [];
        var distances = {};
        var previous = {};
        var bestWay = [];

        mapService
            .getLocations()
            .then(function (locations) {
                var waysPromises = [];
                locations.forEach(function (location) {
                    if (location === origin) {
                        distances[location] = 0;
                        waysToVerify.push({location: location, distance: 0});
                    } else {
                        distances[location] = Number.MAX_VALUE;
                        waysToVerify.push({location: location, distance: Number.MAX_VALUE});
                    }
                    previous[location] = null;
                    waysPromises.push(mapService.findWays(location));
                });
                return Q.all(waysPromises);
            })
            .then(function (results) {
                var ways = {};
                results.forEach(function (result) {
                    for (var location in result) {
                        if (result.hasOwnProperty(location)) {
                            ways[location] = result[location];
                        }
                    }
                });
                while (waysToVerify.length > 0) {
                    waysToVerify = _.orderBy(waysToVerify, ['distance', 'location'], ['asc', 'asc']);

                    var smallest = waysToVerify.shift().location;
                    if (smallest === destination) {
                        while (previous[smallest]) {
                            bestWay.push(smallest);
                            smallest = previous[smallest];
                        }
                        break;
                    }

                    if (!smallest || distances[smallest] === Number.MAX_VALUE) {
                        continue;
                    }

                    for (var way in ways[smallest]) {
                        if (ways[smallest].hasOwnProperty(way)) {
                            var alt = distances[smallest] + ways[smallest][way];

                            if (alt < distances[way]) {
                                distances[way] = alt;
                                previous[way] = smallest;

                                waysToVerify.push({location: way, distance: alt});
                            }
                        }
                    }
                }
                deferred.resolve({'route': bestWay.concat(origin).reverse(), distance: distances[destination]});
            })
            .fail(deferred.reject);
        return deferred.promise;
    }
};