'use strict';

var mapService = require('../../modules/map-service')();
var Q = require('q');

module.exports = function () {
    return {
        createOrUpdateMap: createOrUpdateMap
    };

    function createOrUpdateMap(req, res, next) {
        validateRequest(req)
            .then(function (map) {
                mapService.saveOrUpdate(map);
                res.send(201, 'ok');
                next();
            })
            .fail(function (err) {
                res.send(400, {errors: err});
                next();
            });
    }

    function validateRequest(req) {
        var deferred = Q.defer(), err, map;

        req.assert('origin', 'Invalid origin').notEmpty();
        req.assert('destination', 'Invalid destination').notEmpty();
        req.assert('distance', 'Invalid distance').notEmpty().isInt();

        req.sanitize('distance').toInt();

        err = req.validationErrors(true);
        if (err) {
            deferred.reject(err);
        } else {
            map = {
                origin: req.params.origin,
                destination: req.params.destination,
                distance: req.params.distance
            };
            deferred.resolve(map);
        }
        return deferred.promise;
    }
};
