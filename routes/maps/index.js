'use strict';

var controller = require('./controller')();

module.exports = function (server) {
    server.post('/api/map', controller.createOrUpdateMap);
    server.put('/api/map', controller.createOrUpdateMap);
};
