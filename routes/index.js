'use strict';

var fs = require('fs');
var path = require('path');

module.exports = function (server) {
    fs.readdirSync('./routes').forEach(function (file) {
        if (file !== path.basename(__filename)) {
            require('./' + file)(server);
        }
    });
};
