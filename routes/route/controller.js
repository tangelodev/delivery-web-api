'use strict';

var bestWayFinder = require('./../../modules/best-way-finder')();
var Q = require('q');

module.exports = function () {
    return {
        findRoute: findRoute
    };

    function findRoute(req, res, next) {
        validateRequest(req)
            .then(function (map) {
                return bestWayFinder.findWay(map.origin, map.destination);
            })
            .then(function (route) {
                if (req.params.fuelEconomy && req.params.fuelPrice) {
                    route.costs = route.distance / req.params.fuelEconomy * req.params.fuelPrice;
                }
                res.send(200, route);
                next();
            })
            .fail(function (err) {
                res.send(400, {errors: err});
                next();
            });
    }

    function validateRequest(req) {
        var deferred = Q.defer(), err, map;

        req.assert('origin', 'Invalid origin').notEmpty();
        req.assert('destination', 'Invalid destination').notEmpty();
        if (req.params.fuelEconomy || req.params.fuelPrice) {
            req.assert('fuelEconomy', 'Invalid fuel economy').isFloat();
            req.sanitize('fuelEconomy').toFloat();
            req.assert('fuelPrice', 'Invalid fuel price').isFloat();
            req.sanitize('fuelPrice').toFloat();
        }

        err = req.validationErrors(true);
        if (err) {
            deferred.reject(err);
        } else {
            map = {
                origin: req.params.origin,
                destination: req.params.destination
            };
            deferred.resolve(map);
        }
        return deferred.promise;
    }
};
