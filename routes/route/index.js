'use strict';

var controller = require('./controller')();

module.exports = function (server) {
    server.get('/api/route/:origin/to/:destination', controller.findRoute);
};
