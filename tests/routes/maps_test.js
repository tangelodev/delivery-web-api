'use strict';

var server = require('../../index');
var request = require('supertest');
var should = require('should');

describe('Maps Controller Unit Tests:', function () {
    describe('Testing the POST method', function () {
        it('Should be able to save a new map', function (done) {
            var map = {
                origin: 'São Paulo - SP',
                destination: 'Curitiba - PR',
                distance: 409
            };

            request(server)
                .post('/api/map')
                .set('Accept', 'application/json')
                .send(map)
                .expect('Content-Type', /json/)
                .expect(201, done);
        });
        it('Should be able to validate map without distance information', function (done) {
            var map = {
                origin: 'São Paulo - SP',
                destination: 'Curitiba - PR'
            };

            request(server)
                .post('/api/map')
                .set('Accept', 'application/json')
                .send(map)
                .expect('Content-Type', /json/)
                .expect(400, done);
        });
    });
});