'use strict';

var bestWayFinder = require('../../modules/best-way-finder')();
var should = require('should');

describe('Best Way Finder Unit Tests:', function () {
    describe('#findWay("A", "D")', function () {
        it('Should be able to find route "A" "B" "D" with distance 25km', function (done) {
            this.timeout(10000);
            bestWayFinder.findWay('A', 'D')
                .then(function (bestWay) {
                    bestWay.route.should.be.eql(['A', 'B', 'D']);
                    bestWay.distance.should.be.eql(25);
                    done();
                }).fail(done);
        });
    });
});