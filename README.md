# delivery-web-api
API Web para Sistema de Logística de Entregas.

## Frameworks e Libs
+ [restify](https://github.com/restify/node-restify)
    + Preferi o uso do Restify ao invés do Express por conta de não haver a necessidade do uso templates e renderização de HTML
+ [restify-validator](https://github.com/cjroebuck/restify-validator)
    + Como não estou usando o mongoose, precisava de um meio de garantir que os dados persistidos estejam no formato adequado.
    + É uma biblioteca simples e útil, implementada sobre a biblioteca [node-validator](https://github.com/chriso/validator.js) que é muito estável.
+ [mongojs](https://github.com/mafintosh/mongojs)
    + Preferi o mongojs ao mongoose por conta da velocidade de desenvolvimento. Com o mongojs, executo as mesmas funções que executaria diretamente no banco de dados MongoDB.
+ [supertest](https://github.com/visionmedia/supertest)
    + Framework de testes usado para testar as APIs
+ [should](https://github.com/visionmedia/should.js)
    + Framework de testes para verificaçõs de assertions que segue a técnica do BDD (Behaviour-Driven Development).
+ [q](https://github.com/kriskowal/q)
    + Framework para organização de processamentos assincronos que também nos possibilita tratar possiveis erros desse processamento.
    + Preferi o Q ao Bluebird devido sua simplicidade e ótima documentação
+ [lodash](https://github.com/lodash/lodash)
    + Lib com funções comuns já implementadas

## Pré-requisitos
+ Node.js e NPM
+ MongoDB

## Instalação
```
git clone https://tangelodev@bitbucket.org/tangelodev/delivery-web-api.git
cd delivery-web-api
npm install express-validator
```

## API's

### Mapa de malha logística [/api/map]
Possibilita popular a base de dados o sistema

#### POST [/api/map]
Body Request:
```JavaScript
{
    origin: {origin}, (String)
    destination: {destination}, (String)
    distance: {distance} (Integer)
}
```

### Melhor rota para entrega [/api/route]
Possibilita identificaro menor valor de entrega e seu caminho

#### GET [/api/route/{origin}/to/{destination}?fuelEconomy={fuelEconomy}&fuelPrice={fuelPrice}]
Parametros:
+ **origin**: Origem da rota (Obrigatório)
+ **destination**: Destino da rota (Obrigatório)
+ **fuelEconomy**: Autonomia do caminhão (km/l) (Somente é obrigatório caso o parametro fuelPrice seja informado)
+ **fuelPrice**: Valor do litro do combustível (Somente é obrigatório caso o parametro fuelEconomy seja informado)

Body Response (Exemplo):
```JavaScript
{
    "route": [
        "A",
        "B",
        "D"
    ],
    "distance": 25,
    "costs": 6.25
}
```